function isNumeric(n) {    //check for positive numeric
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function isPosNumeric(n) {    //check for positive numeric
  return !isNaN(parseFloat(n)) && isFinite(n) && n>0;
}

function isInteger(n) {  //check for positive integer
  return n === ~~n;
}

function isLocking() { //lock for arguments without cheking
  return true;
}

function isNumInObj(obj) {  //check numbers in object
  for (const key in obj) {
    if (key === "vertices") continue;  //for task3 check
    if (!isPosNumeric(obj[key])) {
      return false;
    }   
  }
  return true;
}

function checkObjInArr(arr) {  //check numbers in object
  for (let i = 0; i < arr.length; i++) {
     if (!isNumInObj(arr[i])) {
       return false;
     }      
  }
  return true;
}

//Decorator
function typeCheck(f, check, findReason) {  //Find reason: 1 - int; 2 - positive number 
  return function () {
    for (let i = 0; i < arguments.length; i++) {
      if (!check[i](arguments[i])) {
        switch (findReason) {
          case 1:
            return {
              status: "Invaild argument: " + i, 
              reason: "Enter positive integer"
            };
          case 2:
            return {
              status: "Invaild argument: " + i, 
              reason: "Enter positive number"
            };
        }
      }
    }
    return f.apply(this, arguments);
  }
}

//task1 
writeDesk = typeCheck(writeDesk, [isPosNumeric, isPosNumeric, isLocking], 1); //decorator check pos numeric
writeDesk = typeCheck(writeDesk, [isInteger, isInteger, isLocking], 1);      //decorator check integer
console.log(writeDesk(10, 3, 5));


//task2 
checkEnvelope = typeCheck(checkEnvelope, [isNumInObj, isNumInObj], 2);
console.log(checkEnvelope({a: 48, b: 2}, {c: 40, d:30}));


//task3 
let arr = [{vertices: 'ABC', a: 10, b: 20, c: 22.36}, 
           {vertices: 'def', a: 8, b: 22, c: 22.36}, 
           {vertices: 'ghi', a: 20, b: 7.2, c: 22.36}];

    
sortTriangle = typeCheck(sortTriangle, [checkObjInArr], 2);
console.log("Task3:Sorted triangles: " + sortTriangle(arr));


//task4 
findPolyndrom = typeCheck(findPolyndrom, [isNumeric], 1);
console.dir("Task4:Polyndrom is: " + findPolyndrom(755641234432153));


//task5 
let context = {
min: 100000,
max: 200024
};
findLuckyTikets = typeCheck(findLuckyTikets, [isNumInObj], 2);
console.log(findLuckyTikets(context));

//task6 
rowNumbers = typeCheck(rowNumbers, [isPosNumeric, isPosNumeric], 1);
rowNumbers = typeCheck(rowNumbers, [isInteger, isLocking], 1);
console.log("Task6: Numbers is " + rowNumbers(20, 51));


//task7 
context.length = 6;
writeFibomachi = typeCheck(writeFibomachi, [isNumInObj], 1);
console.log("Task7: Fibo numbers is " + writeFibomachi(context));
