

function writeFibomachi(obj) {

  let a = 1; // fibo number(n-2)
  let b = 1; // fibo number(n-1)
  let arr = []; //array with Fibo numbers
  let lengthInObj = false; //has lenth key in obj
  let minInObj = false; //has min key in obj
  let maxInObj = false; //has max key in obj

  for (const key in obj) { //check method of filter fibo
    if (key === "length") {
      lengthInObj = true;
      break;
    } else if (key === "min") {
      minInObj = true;
    } else if (key === "max") {
      maxInObj = true;
    }
  };

  if (lengthInObj) { //method which filter fibo numbers by lenth
    arr.push(a);
    arr.push(b);
    for (let i = 0; b.toString().split("").length <= obj.length; i++) { 
      arr.push(a+b);  //get last Fibo number
      a = b;          //set new (n-2) Fibo number
      b = arr[(arr.length -1)];  //set new (n-1) number
    };
    
    arr = arr.filter(function (num) {
      return ( num.toString().split("").length === obj.length);
    });
  }  
    
  if (minInObj & maxInObj) { //method which filter range of fibo numbers
      arr.push(a);
      arr.push(b);
      while (arr[(arr.length -1)] <= obj.max) {
        arr.push(a+b);  //get last Fibo number
        a = b;          //set new (n-2) Fibo number
        b = arr[(arr.length -1)];  //set new (n-1) number
      }
   
      arr = arr.filter(function (num) {
        return (num >= obj.min && num <= obj.max)
      });
  }  
  
  return arr;
    
}


