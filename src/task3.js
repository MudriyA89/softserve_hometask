

function sortTriangle(arr) {

  let newArr= []; //arr with names of triangles
  for (let i = 0; i < arr.length; i++) {
    let p = (arr[i].a + arr[i].b +arr[i].c)/2; //find half of perimeter
    
    newArr[i] = {}; 
    newArr[i].S = Math.sqrt(p*(p-arr[i].a)*   //fing Square of each triangle
                              (p-arr[i].b)*
                              (p-arr[i].c)); 
    
    newArr[i].vertices = arr[i].vertices;
  }
  
  newArr.sort(function (a, b) {   //sort triangles on square
    if (a.S < b.S) return 1;
    return -1;  
    });
  
  
  for (let i = 0; i < newArr.length; i++) { //form answer
    newArr[i] = newArr[i].vertices;
  };
  
  return newArr;
  };

