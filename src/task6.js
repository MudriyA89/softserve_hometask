

function rowNumbers(n, m) {
  let startNumber = Math.ceil(Math.sqrt(m)); //check for min value of  numbers
  let arr = []; //arr for numbers
  for (let i = 0; i < n; i++) { 
      arr.push(startNumber+i);
 }
 return arr.join(", "); //create string of numbers
}


