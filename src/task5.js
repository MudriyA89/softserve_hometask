
function findLuckyTikets(obj) {
    
  let resultObj = {
    countEasy: 0,
    countHard: 0,
    winner: ""
  };

  for (let i = obj.min; i <= obj.max ; i++) { //
      easy(i);
      hard(i);
    }
   
    //Easy method for count
    function easy(arr) {  
      let left =0; 
      let right = 0;
      arr = arr.toString().split("");
      
      left = arr.slice(0, arr.length/2).reduce(function (a, b) { //count left side of ticket numbers
        return +a + +b;
      })
  
      right = arr.slice(arr.length/2).reduce(function (a, b) {  //count right side of ticket numbers
        return +a + +b;
      });
  
      if (left == right) {    //check if left and right side are equally
        resultObj.countEasy += 1;
      }
      
    }
    
    //hard method for count
    function hard(arr) { 
      arr = arr.toString().split(""); 
      evenCount = 0; //count even numbers of ticket
      oddCount = 0;  //count odd numbers of ticket
      
      for (let i = 0; i < arr.length; i++) {
        
        if (arr[i] % 2 == 0 ) { 
          evenCount += +arr[i]; 
        } else { 
          oddCount +=  +arr[i]
        }
       
        if (evenCount == oddCount) { //check if even and odd numbers are equally
          resultObj.countHard += 1;
        }
      }           
    }   

    if (resultObj.countEasy > resultObj.countHard) { //check and write result in object
        resultObj.winner = "Winner is countEasy :" + resultObj.countEasy;
    } else resultObj.winner = "Winner is countHard :" + resultObj.countHard ;
                
  return resultObj;
  
}


