


function findPolyndrom(str) {
  let arr = str.toString().split("");
  let part;  //checked part of str
  const minLenthPolyndrom = 3; 
  
  if (isPolyndrom(arr)) { //check if we already have polyndrom
    return arr.join("");
  }

  //find polyndrom in part of str
  for (let i = 0; i < arr.length; i++) { 
    
    //incrase part of str to check is polyndrom
    for (let i = 0; i < arr.length-(minLenthPolyndrom-1); i++) {
      part = arr.slice(0, i + minLenthPolyndrom); 
      if (isPolyndrom(part)) return part.join("");
    }
  
  arr.shift(); //cut one number of input 

  }

  //function for polyndrom check
  function isPolyndrom(part) {  
    part.reverse();  
    if (part.join("") === part.reverse().join("") ) {
      return true;
    }
  }

  return 0; //polyndrom not found
}








