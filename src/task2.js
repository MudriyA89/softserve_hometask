

function checkEnvelope(env1, env2) {
  let arr1 = []; //first env 
  let arr2 = []; //second env
  let S1; 
  let S2;
  let a, b, c, d;
  for (const key in env1) {
    arr1.push(env1[key])
    };

  for (const key in env2) {
    arr2.push(env2[key])
    };
  
  arr1 = arr1.sort(function (a, b) {
    return a-b;
  });  
  a = arr1[0];
  b = arr1[1];

  arr2 = arr2.sort(function (a, b) {
    return a-b;
  });  
  c = arr2[0];
  d = arr2[1];
  
  //firsly we need to find the biggest evelope
  S1 = a * b;   //find square of first envelope
  S2 = c * d;   //find square of second envelope
  
  //check the biggest evelope
  if (S1 > S2) {
    if ((a > c && b > d) || (a > c && b < d &&
        Math.pow((b+a)/(d+c), 2) + Math.pow((b-a)/(d-c), 2) >= 2))  {
          return 1;
    }
       
  } else if ((c > a && d > b) || (c > a && d < b && 
              Math.pow((d+c)/(b+a), 2) + Math.pow((d-c)/(b-a), 2) >= 2)) { 
      return 2;
    } 

  return 0;
}
