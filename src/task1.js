function writeDesk(x,y, symbol) {
  let desk = "";
  for (let i = 0; i < y; i++) {
      for (let j = 0; j < x*2; j++) {   // we use "x*2" because chess deck has symbol+gap 
        let checkLine = i%2;
        switch (checkLine) {
          case 0:  //even string
            if (j%2 == 0) {
              desk += symbol;
            } else {
              desk += " ";
            }
            break;
        
          case 1:   //odd string
            if (j%2==0) {
              desk += " ";
            } else {
              desk += symbol;
            }
            break;
        }
      }
      desk += '\n';
  }
  return desk;
}
